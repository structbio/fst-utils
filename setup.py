from setuptools import setup

setup(name='fastaUtils',
      version='3.0',
      description='fastaUtils',
      url='https://gitlab.com/LBS-EPFL/fastautils',
      author='szamunerd',
      author_email='zimmerzam@gmail.com',
      license='MIT',
      packages=['fastaUtils'],
      zip_safe=False)
