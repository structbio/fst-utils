#!/usr/bin/env bats

@test "Check exit code when inputs with same number of sequences" {
  run fst-paste data/DJB11.fasta data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check exit code when inputs with different number of sequences" {
  run fst-paste data/DJBX_human.fasta data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check working" {
  result="$(fst-paste data/DJB11.fasta data/DJB11.fasta | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 716 ]
}

@test "Check number of sequences in output" {
  result="$(fst-paste data/DJB11.fasta data/DJB11.fasta | grep -c '>')"
  [ "$result" -eq 5 ]
  result="$(fst-paste data/DJB11.fasta data/DJBX_human.fasta | grep -c '>')"
  [ "$result" -eq 4 ]
  result="$(fst-paste data/DJBX_human.fasta data/DJB11.fasta | grep -c '>')"
  [ "$result" -eq 4 ]
}

@test "Check correctness: input from stdin" {
  result="$(cat data/DJB11.fasta | fst-paste data/DJB11.fasta - | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 716 ]
}

@test "Check correctness: multiple inputs from stdin raise an error" {
  run fst-paste data/DJB11.fasta - -
  [ "$status" -eq 1 ]
}
