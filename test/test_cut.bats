#!/usr/bin/env bats

@test "Check exit code when in direct mode" {
  run fst-cut data/DJB11.fasta -c 1
  [ "$status" -eq 0 ]
  run fst-cut data/DJB11.fasta -c 1-3
  [ "$status" -eq 0 ]
  run fst-cut data/DJB11.fasta -c 1-3 4
  [ "$status" -eq 0 ]
  run fst-cut data/DJB11.fasta -c 4-
  [ "$status" -eq 0 ]
}

@test "Check exit code when in reverse mode" {
  run fst-cut data/DJB11.fasta -c 1 -r
  [ "$status" -eq 0 ]
  run fst-cut data/DJB11.fasta -c 1-3 -r
  [ "$status" -eq 0 ]
  run fst-cut data/DJB11.fasta -c 1-3 4 -r
  [ "$status" -eq 0 ]
  run fst-cut data/DJB11.fasta -c 4- -r
  [ "$status" -eq 0 ]
}

@test "Check sequence length when in direct mode" {
  result="$(fst-cut data/DJB11.fasta -c 1 | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 357 ]
  result="$(fst-cut data/DJB11.fasta -c 1-3 | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 355 ]
  result="$(fst-cut data/DJB11.fasta -c 1-3 4 | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 354 ]
  result="$(fst-cut data/DJB11.fasta -c 4- | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 3 ]
}

@test "Check sequence length when in reverse mode" {
  result="$(fst-cut data/DJB11.fasta -c 1 -r | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 1 ]
  result="$(fst-cut data/DJB11.fasta -c 1-3 -r | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 3 ]
  result="$(fst-cut data/DJB11.fasta -c 1-3 4 -r | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 4 ]
  result="$(fst-cut data/DJB11.fasta -c 4- -r | fst-awk 'NR==0 {print(len(seq.seq))}')"
  [ "$result" -eq 355 ]
}

