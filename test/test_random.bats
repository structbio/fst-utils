@test "Check correctness: profile is conserved" {
  result="$(sed 's/ /\n/g' outputs/profile.txt | sed 's/:/ /g' | awk '{print $2}' > outputs/freq.txt)"
  result="$(fst-random outputs/profile.txt 10000  | fst-profile | sed 's/ /\n/g' | sed 's/:/ /g' | awk '{print $2}' > outputs/freq_random.txt)"
  result="$(paste outputs/freq.txt outputs/freq_random.txt | awk '{a+=sqrt(($1-$2)*($1-$2)); n+=1} END{if(a/n<0.01) {print 1} else {print 0} }')"
  [ "$result" -eq 1 ]
}

